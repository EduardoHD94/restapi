<?php

	/**
	* 
	*/
	class historial 
	{
		const NOMBRE_TABLA = "Historial";
		const ID_HISTORIAL = "idHistorial";
		const ID_USUARIO = "idUsuario";
		const LONG_URL = "longURL";

	
    const CODIGO_EXITO = 1;
    const ESTADO_EXITO = 1;
    const ESTADO_ERROR = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_ERROR_PARAMETROS = 4;
    const ESTADO_NO_ENCONTRADO = 5;
    const ESTADO_NO_VALID_URL = 6;
		

		public static function post($peticion)
		{
			$idUsuario = usuarios::autorizar();

	        $body = file_get_contents('php://input');
	        $historial = json_decode($body);

	         $idHistorial = historial::crear($idUsuario, $historial);
	        
	        if ($idHistorial === "false") {
	        	http_response_code(200);
		        return [
		            "estado" => self::ESTADO_NO_VALID_URL,
		            "mensaje" => "URL No válida",
		       ];
	        }else{
		        http_response_code(201);
		        return [
		            "estado" => self::CODIGO_EXITO,
		            "mensaje" => "URL creada",
		       ];

		   	} 
	    	
	    	
		}
		
		public static function get($peticion)
		{
			$idUsuario = usuarios::autorizar();

			  if (empty($peticion[0]))
	            return  self::obtenerHistorial($idUsuario);
		      else
		      	return self::obtenerHistorial($idUsuario, $peticion[0]);
		}

		public static function delete($peticion)
		{
			$idUsuario = usuarios::autorizar();
			
		}


		public function obtenerHistorial($idUsuario, $idHistorial = NULL)
		{
		        try {
		            if (!$idHistorial) {
		                $comando = "SELECT * FROM " . self::NOMBRE_TABLA .
		                    " WHERE " . self::ID_USUARIO . "=?";

		                // Preparar sentencia
		                $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
		                // Ligar idUsuario
		                $sentencia->bindParam(1, $idUsuario, PDO::PARAM_INT);

		            } else {
		                $comando = "SELECT * FROM " . self::NOMBRE_TABLA .
		                    " WHERE " . self::ID_HISTORIAL . "=? AND " .
		                    self::ID_USUARIO . "=?";

		                // Preparar sentencia
		                $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
		                // Ligar idContacto e idUsuario
		                $sentencia->bindParam(1, $idHistorial);//, PDO::PARAM_INT);
		                $sentencia->bindParam(2, $idUsuario);//, PDO::PARAM_INT);
		            }

		            // Ejecutar sentencia preparada
		            if ($sentencia->execute()) {
		                http_response_code(200);
		                return
		                    [
		                        "estado" => self::ESTADO_EXITO,
		                        "datos" => $sentencia->fetchAll(PDO::FETCH_ASSOC)
		                    ];
		            } else
		                throw new ExcepcionApi(self::ESTADO_ERROR, "Se ha producido un error");

		        } catch (PDOException $e) {
		            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
		        }
		    }


		public function crear($idUsuario, $historial)
	    {
	        if ($historial)
	        {

	        	if (!historial::validateURL($historial->longURL)) {
	        		return "false";	
	        	}
	        	else
	            try {


	            	//	echo json_encode($historial);
	                $pdo = ConexionBD::obtenerInstancia()->obtenerBD();

	                
	                $comando = "INSERT INTO " . self::NOMBRE_TABLA . " ( " .
                    self::LONG_URL . "," .
                    self::ID_HISTORIAL . "," .
                    self::ID_USUARIO . ")" .
                    " VALUES(?,?,?)";

                    $a = $historial->longURL;
	                // Preparar la sentencia
	                $sentencia = $pdo->prepare($comando);
	                $sentencia->bindParam(1, $a);
	                $sentencia->bindParam(2, $S);
	                $sentencia->bindParam(3, $idUsuario);


	        
	                $sentencia->execute();

	                // Retornar en el último id insertado
	                return $pdo->lastInsertId();

	            } catch (PDOException $e) {
	                throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
	            }
	        } else {
	            throw new ExcepcionApi(
	                self::ESTADO_ERROR_PARAMETROS,
	                utf8_encode("Error en existencia o sintaxis de parámetros"));
	        }

	    }


	    public function validateURL($url)
	    {
	    	if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
			    return true;
			} else {
			    return false;
			}	 
	   }
	}