<?php



/**
* 
*/
class tinyurl
{

	CONST NOMBRE_TABLA ="Tiny";
	CONST ID_TINY = "idTiny";
	CONST TINY = "urlTiny";
	CONST LONG = "longTiny";
	CONST ID_USUARIO = "idUsuario";
	const TINYURL = "http://localhost/bharat/git/tiny-url-with-php";
	const CODIGO_EXITO = 1;
    const ESTADO_EXITO = 1;
    const ESTADO_ERROR = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_ERROR_PARAMETROS = 4;
    const ESTADO_NO_ENCONTRADO = 5;
	
	public static function get($peticion)
    {
        $idUsuario = usuarios::autorizar();

        if (empty($peticion[0]))
            return  self::obtenerTinyUrls($idUsuario);
        else
          return self::obtenerTinyUrls($idUsuario, $peticion[0]);

    }

    public static function post($peticion)
    {
        $idUsuario = usuarios::autorizar();

        $body = file_get_contents('php://input');
        $tinyURL = json_decode($body);

        $idContacto = tinyurl::MakeTinyURL($idUsuario, $tinyURL);

        http_response_code(201);
        return [
            "estado" => self::CODIGO_EXITO,
            "mensaje" => "Tiny creado",
            "id" => $idContacto
        ];

    }

    private function obtenerTinyUrls($idUsuario, $idTiny=NULL)
    {
    	try{
    		if (!$idTiny)
    		{
    			$comando = "SELECT * FROM " . self::NOMBRE_TABLA ." WHERE " . self::ID_USUARIO . "=?";
				$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
				$sentencia->bindParam(1, $idUsuario, PDO::PARAM_INT);
    		}
    		else
    		{

    			$comando= tinyurl::GetTinyURL($idUsuario,$idTiny);

				$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);


    			
				//$sentencia->bindParam(1, $idUsuario);
				//$sentencia->bindParam(2, $idTiny);
    		}

    		// Ejecutar sentencia preparada
            if ($sentencia->execute()) {

			@$data = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			echo @$data[self::LONG];

                http_response_code(200);
                return
                    [
                        "estado" => self::ESTADO_EXITO,
                        "datos" => $sentencia->fetchAll(PDO::FETCH_ASSOC)
                    ];
            } else
                throw new ExcepcionApi(self::ESTADO_ERROR, "Se ha producido un error");


    	}catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
        }	
    }


	private function MakeTinyURL($idUsuario, $tinyURL)
	{
 		if ($tinyURL) {
            try {
                $pdo = ConexionBD::obtenerInstancia()->obtenerBD();

                // Sentencia INSERT
                $comando = "INSERT INTO " . self::NOMBRE_TABLA . " ( " .
                    self::LONG . "," .
                    self::TINY . "," .
                    self::ID_USUARIO . ")" .
                    " VALUES(?,?,?)";

                // Preparar la sentencia
                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $longURL);
                $sentencia->bindParam(2, $tiny);
                $sentencia->bindParam(3, $idUsuario);




                $longURL = $tinyURL->longURL;  
                $last_id = $pdo ->prepare("select max(idTiny) as max_id from ". self::NOMBRE_TABLA)->execute();
                $tiny = self::TINYURL."?".tinyurl::EcryptString($last_id['max_id']+1);
           

				$ch = curl_init($longURL);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				curl_exec($ch);
				$data = curl_getinfo($ch);

			print_r($data = curl_getinfo($ch));

				echo $data['url'];


                $sentencia->execute();

                // Retornar en el último id insertado
                return $pdo->lastInsertId();

            } catch (PDOException $e) {
                throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
            }
        } else {
            throw new ExcepcionApi(
                self::ESTADO_ERROR_PARAMETROS,
                utf8_encode("Error en existencia o sintaxis de parámetros"));
        }
	}


	private function GetTinyURL($idUsuario, $id){
		
		$tiny_url_id = tinyurl::DecryptString($id);

		$comando = "SELECT ".self::LONG." from ". self::NOMBRE_TABLA." where ".self::ID_TINY.  " = ".$tiny_url_id. " AND ". self::ID_USUARIO. "= ".$idUsuario;
		return $comando;



		/*

		if(@$data[self::LONG]!="") {
			@header("location:".$data[self::LONG]);
			exit;
		} else {
			@header("location:example.php");
			exit;
		}*/
	}

	private function EcryptString($string){
		$search_array = array(0,1,2,3,4,5,6,7,8,9);
		$replace_array = array("a","b","c","d","e","f","g","h","i","j");
		
		return str_replace($search_array,$replace_array,$string);		
	}

	private function DecryptString($string){
		$search_array = array(0,1,2,3,4,5,6,7,8,9);
		$replace_array = array("a","b","c","d","e","f","g","h","i","j");
		
		return str_replace($replace_array,$search_array,$string);		
	}


	public function get_redirect_target($url)
	{
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_HEADER, 1);
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $headers = curl_exec($ch);
	    curl_close($ch);
	    // Check if there's a Location: header (redirect)
	    if (preg_match('/^Location: (.+)$/im', $headers, $matches))
	        return trim($matches[1]);
	    // If not, there was no redirect so return the original URL
	    // (Alternatively change this to return false)
	    return $url;
	}
}